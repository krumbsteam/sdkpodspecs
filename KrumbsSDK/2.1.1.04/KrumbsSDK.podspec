#
# Be sure to run `pod lib lint KrumbsSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "KrumbsSDK"
  s.version          = "2.1.1.04"
  s.summary          = "The iOS library of Krumbs SDK - the community engagement & micro-reporting platform"
  s.description      = <<-DESC
          Krumbs SDK offers the ability for application developers to build consumer-friendly micro-reporting apps, that includes direct engagement between reporter and responder
                       DESC

  s.homepage         = "https://krumbs.net"
  s.license          = {:text => "See https://krumbs.net/sdk/LICENSE", :type => "Commercial" }
  s.author           = { "Krumbs Team" => "sdkgroup@krumbs.net" }

  s.platform     = :ios, '9.0'
  s.requires_arc = true
  s.source           = { :http => "https://bitbucket.org/krumbsteam/krumbs-sdk-samples/downloads/KrumbsSDK_iOS_Pod-#{s.version}.tar.gz"}

  s.ios.vendored_frameworks = ["KrumbsSDK.framework"]
  s.header_mappings_dir = "KrumbsSDK.framework/Headers"
  s.preserve_paths = ["**/*"]
  s.module_map = "KrumbsSDK.framework/Modules/module.modulemap"
  s.source_files = 'KrumbsSDK.framework/Headers/*.h'
  s.public_header_files = 'KrumbsSDK.framework/Headers/*.h'
  s.ios.frameworks = ['UIKit','Foundation','CoreData','MobileCoreServices','CoreLocation','CoreImage','ImageIO','EventKit','Photos','AssetsLibrary']
  
end
